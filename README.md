# Gestion fournisseur :

## gestion-fournisseur : application principale
L'application gérant client, articles et commandes.

### Fichiers d'entrée
* FichierCommande.csv          - Un fichier stockant la commande d'un client
* Accuse-Recept-Entree.txt     - Fichier template pour la génération d'un accusé de récéption
* Fichier-Facture-Entree.txt   - Fichier template pour la génération d'une facture

### Fichiers de sortie
* Accuse-Recept-Sortie.txt     - Accusé de récéption rempli avec les données d'une commande
* Fichier-Facture-Sortie.txt   - Facture rempli avec les données d'une commande

## batch-fournisseur : application batch
Une application qui s'éxécute toutes les heures pour évaluer le stock et mettre en avant les articles dont le stock est critique.

### Setup du batch
* batch-fournisseur-setup.bat   - met à jour le planificateur des tâches pour éxécuter batch_fournisseur.exe toutes les heures

### Exécutables
* batch_fournisseur.exe         - éxécute le programme batch

### Fichiers de sortie
* StatutStock.txt               - Fichier récapitulatif du stock
