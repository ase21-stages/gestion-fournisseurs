       program-id. Program1 as "batch_fournisseur.Program1".
       
       environment division.
       input-output section.
           SELECT F-StatutStock ASSIGN TO "C:\Users\formation10\Documents\stages\gestion-fournisseurs\StatutStock.txt"
               STATUS StatusCode ORGANIZATION LINE SEQUENTIAL ACCESS SEQUENTIAL.              
       data division.
       file section.
       FD F-StatutStock RECORD VARYING FROM 0 TO 255.
       01 E-StatutStock PIC X(255).

       working-storage section.       
       01 ARTICLE.
         10 ID_ARTICLE sql char-varying (36).
         10 NOM sql char-varying (50).
         10 PRIX      PIC 9(10)v99.
         10 STOCK     PIC 9(10).
         10 STOCK_MIN PIC 9(10).
       
       01 ARTICLE-ENTETE.
         10 filler PIC X(38) value " Référence article".
         10 filler PIC X(3) value " | ".
         10 filler PIC X(50) value " NOM ".
         10 filler PIC X(3) value " | ".
         10 filler PIC X(14) value " PRIX ".
         10 filler PIC X(3) value " | ".
         10 filler PIC X(10) value " STOCK ".
         10 filler PIC X(3) value " | ".
         10 filler PIC X(10) value " STOCK MIN".

       01 ARTICLE-DISPLAY.
         10 ID_ARTICLE PIC X(36).
         10 filler PIC X(3) value " | ".
         10 NOM PIC X(50).
         10 filler PIC X(3) value " | ".
         10 PRIX   PIC z(9)9v,99.
         10 filler PIC X(6) value "€ | ".
         10 STOCK  PIC z(9)9.
         10 filler PIC X(3) value " | ".
         10 STOCK_MIN PIC z(9)9.
       
       77 Valeur-Stock-Display PIC z(9)9v,99.
       77 Valeur-Stock PIC 9(10)v99.

       77 Nombre-Article-Sous-Min-Display PIC z(9)9.
       77 Nombre-Article-Sous-Min PIC 9(10).

       77 Ligne-Separation PIC X(132) value all "-".

       77 StatusCode PIC 99.

       77 EOT PIC 9.

       77 ConnexionDB string.

       exec sql
           include SQLCA
       end-exec.

       procedure division.
           perform INIT-CONNEXION.
           
           open output F-StatutStock.
           write E-StatutStock from "Statut du stock : ".

           perform SELECT-STOCK-VALUE.
           move Valeur-Stock to Valeur-Stock-Display.
           
           write E-StatutStock from " ".

           string " Valeur : " Valeur-Stock-Display "€" delimited by size into E-StatutStock.
           write E-StatutStock.

           write E-StatutStock from " ".

           perform COUNT-ARTICLE-SOUS-MIN
           move Nombre-Article-Sous-Min to Nombre-Article-Sous-Min-Display.
           string " Nombre de produits sous le seuil minimum : " Nombre-Article-Sous-Min-Display delimited by size into E-StatutStock.
           write E-StatutStock.
           write E-StatutStock from Ligne-Separation.
           write E-StatutStock from ARTICLE-ENTETE.
           write E-StatutStock from Ligne-Separation.

           move 0 to EOT.
           perform INIT-CURSOR-ARTICLE-SOUS-MIN.
           perform until EOT = 1
               perform FETCH-CURSOR-ARTICLE-SOUS-MIN
               if SQLCODE = 0 or SQLCODE = 1 then
                   move corresponding ARTICLE to ARTICLE-DISPLAY
                   write E-StatutStock from ARTICLE-DISPLAY
               else 
                   move 1 to EOT
               end-if
           end-perform.
           goback.

       INIT-CONNEXION.
           move
             "Trusted_Connection=yes;Database=GestionFournisseur;server=PO-FO10\SQLEXPRESS01;factory=System.Data.SqlClient;"
             to ConnexionDB.

           exec sql
             connect using :ConnexionDB
           end-exec.
           if (sqlcode not equal 0) then
               stop run
           end-if.

           exec sql
             set autocommit on
           end-exec.
           

           SELECT-STOCK-VALUE.
               exec sql
                 SELECT SUM(PRIX * STOCK) into :Valeur-Stock FROM ARTICLE
               end-exec.

           COUNT-ARTICLE-SOUS-MIN.
               exec sql
                 SELECT COUNT(*) INTO :Nombre-Article-Sous-Min FROM ARTICLE
                 WHERE STOCK_MIN > STOCK
               end-exec.

           INIT-CURSOR-ARTICLE-SOUS-MIN.
               exec sql
                 DECLARE ARTICLE-SOUS-MIN-CURSOR CURSOR FOR 
                 SELECT * FROM ARTICLE
                 WHERE STOCK_MIN > STOCK
               end-exec.

               exec sql
                 OPEN ARTICLE-SOUS-MIN-CURSOR
               end-exec.

           FETCH-CURSOR-ARTICLE-SOUS-MIN.
               exec sql
                 FETCH ARTICLE-SOUS-MIN-CURSOR INTO :ARTICLE
               end-exec.

           END-CURSOR-ARTICLE-SOUS-MIN.
               exec sql
                 CLOSE ARTICLE-SOUS-MIN-CURSOR
               end-exec.

           
           
       end program Program1.
