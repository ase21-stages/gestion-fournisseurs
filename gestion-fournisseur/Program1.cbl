       program-id. Program1 as "gestion_fournisseur.Program1".
       
       environment division.
       input-output section.
           SELECT F-Commande ASSIGN TO "C:\Users\formation10\Documents\stages\gestion-fournisseurs\FichierCommande.csv"
               STATUS StatusCode ORGANIZATION LINE SEQUENTIAL ACCESS SEQUENTIAL.
               
           SELECT F-Reception-Entree ASSIGN TO "C:\Users\formation10\Documents\stages\gestion-fournisseurs\Accuse-Recept-entree.txt"
               STATUS StatusCode ORGANIZATION LINE SEQUENTIAL ACCESS SEQUENTIAL.  

           SELECT F-Reception-Sortie ASSIGN TO "C:\Users\formation10\Documents\stages\gestion-fournisseurs\Accuse-Recept-Sortie.txt"
               STATUS StatusCode ORGANIZATION LINE SEQUENTIAL ACCESS SEQUENTIAL.  

           SELECT F-Facture-Entree ASSIGN TO "C:\Users\formation10\Documents\stages\gestion-fournisseurs\Fichier-Facture-Entree.txt"
               STATUS StatusCode ORGANIZATION LINE SEQUENTIAL ACCESS SEQUENTIAL.           

           SELECT F-Facture-Sortie ASSIGN TO "C:\Users\formation10\Documents\stages\gestion-fournisseurs\Fichier-Facture-Sortie.txt"
               STATUS StatusCode ORGANIZATION LINE SEQUENTIAL ACCESS SEQUENTIAL.        
              
       data division.
       file section.
       FD F-Commande RECORD VARYING FROM 0 TO 255.
       01 E-Commande PIC X(255).

       FD F-Reception-Entree RECORD VARYING FROM 0 TO 255.
       01 E-Reception-Entree PIC X(255).

       FD F-Reception-Sortie RECORD VARYING FROM 0 TO 255.
       01 E-Reception-Sortie PIC X(255).

       FD F-Facture-Entree RECORD VARYING FROM 0 TO 255.
       01 E-Facture-Entree PIC X(255).

       FD F-Facture-Sortie RECORD VARYING FROM 0 TO 255.
       01 E-Facture-Sortie PIC X(255).

       working-storage section.
       
      ***********************
      *TABLES BASE DE DONNEE*
      ***********************
       01 CLIENT.
         10 ID_CLIENT sql char-varying (36).
         10 NOM sql char-varying (50).
         10 PRENOM sql char-varying (50).
         10 INTITULE sql char-varying (10).
         10 TELEPHONE sql char (10).
         10 MAIL sql char-varying (50).
         10 ADRESSE sql char-varying (50).
         10 CODE_POSTAL sql char (5).
         10 VILLE sql char-varying (50).
       
       77 NombreClient PIC 9(9).
      
       01 ARTICLE.
         10 ID_ARTICLE sql char-varying (36).
         10 NOM sql char-varying (50).
         10 PRIX      PIC 9(10)v99.
         10 STOCK     PIC 9(10).
         10 STOCK_MIN PIC 9(10).

       77 NombreArticle PIC 9(5).

       01 COMMANDE.
         10 ID_COMMANDE sql char-varying (36).
         10 ID_CLIENT sql char-varying (36).
         10 ID_ARTICLE sql char-varying (36).
         10 QUANTITE      PIC 9(10).
         10 PRIX          PIC 9(10)v99.
         10 DATE_COMMANDE PIC X(10).


       01 TABLEAU PIC X(350) OCCURS 14 TIMES.
 
      ** Declaration pour la date :
       01 DateSysteme.
         10 Annee PIC 99.
         10 Mois  PIC 99.
         10 Jour  PIC 99.

      ** Test pour la recherche d'un article
       01 TEST_ARTICLE.
         10 TEST_PRIX PIC X.
         10 TEST_STOCK PIC X.
         10 TEST_STOCK_MIN PIC X.
         10 TEST_STOCK_SUP_MIN PIC X.
         10 TEST_STOCK_INF_MIN PIC X.
       
      ** Declaration pour la liste d'Article sur Accuse de reception ou Facture
       
       01 LISTE-ARTICLES. 
         10 Nom PIC X(29).
         10 Filler PIC x(4).
         10 Quantite PIC z(5)9.
         10 Filler PIC x(6).
         10 Prix PIC z(9)9v,99.
         10 Filler PIC X(3) value "�".
         10 Filler PIC x(2). 
         10 Total PIC z(5)9v,99.
         10 Filler PIC x(3) value "�".
      
      ** Fin de page sur l'accuse ou la facture

       01 LigneBasDePage.
         10 Filler PIC XX.
         10 Filler PIC x(4) value "EKIA".
         10 Filler PIC x(13).
         10 FIller pic x(32) value "Les meubles aux noms ubuesques.".
         10 Filler pic x(13).
         10 Filler PIC x(5) value "Page ".  
         10 NumeroPage pic 99.

       
       01 LigneEtoiles.
           10 Filler pic x(73) value all "*".

       01 LigneEntete.
           10 Filler pic x(25) value "ARTICLES ".
           10 filler pic x(5).
           10 filler pic x value "*".
           10 filler pic x(12) value "  QUANTITE  ".
           10 filler pic x value "*".
         10 filler pic x(19) value "   PRIX UNITAIRE   ".
           10 filler pic x value "*".
         10 filler pic x(10) value "   TOTAL  ".

     
       77 Option         PIC XX.
       77 OptionNUm      PIC 99.

       77 EOT            PIC 9.
       77 EOF            PIC 9.

       77 NumeroLigne    PIC 99.
       77 LigneTableau   PIC 99.
       77 StatusCode     PIC 99.
       77 Annuler        PIC X.

       77 Total-Commande PIC 9(10)v99.
       77 Total-Commande-Display PIC z(9)9v,99.
       77 NomPrenom      PIC X(40).

       77 LastZone       PIC X(255).
       77 NoPage         PIC 99.
       77 NbLigne        PIC 99.
       77 MaxLigne       PIC 99 value 36.
    
      ** Declaration pour les couleurs du fond et des carac�res  :
       77 CouleurFond     PIC 99 value 1.
       77 CouleurCara     PIC 99 value 7.

      ** Declaration du titre des �crans
       77 S-Titre         PIC X(34).
       77 S-SousTitre     PIC X(34).
       77 S-Message       PIC X(70).

      ** Declaration de test de valeur
       77 EstValide       PIC 9.
       77 EstLigneArticle PIC 9.

      ** Declaration des variables d'impression (Replace)
       77 R-NomClient     PIC X(40) value "$NomClient".
       77 R-Adresse       PIC X(50) value "$Adresse".
       77 R-CodePostal    PIC X(5) value "$CP".
       77 R-Ville         PIC X(50) value "$Ville".
       77 R-ID-Client     PIC X(36) value "$ID-Client".
       77 R-ID-Commande   PIC X(36) value "$ID-Commande".
       77 R-Jour          PIC X(2) value "$J".
       77 R-Mois          PIC X(2) value "$M".
       77 R-Annee         PIC X(2) value "$A".
       77 R-Date-Commande PIC X(10) value "$DateCmd".
       77 R-Liste-Article PIC X(74) value "$listeArticle".
       77 R-Total         PIC X(13) value "$total".
       
     
      ** Declarations SQL :
       77 SQL-index-cursor PIC 9(9).
       77 SQL-newIndex-cursor PIC 9(9).
       01 SQL-update-cursor PIC X.
         88 SQL-update-cursor-TRUE value 'Y'.
         88 SQL-update-cursor-FALSE value 'N'.  
      
       77 ConnexionDB string.

           exec sql
               include sqlca
           end-exec.

      *******************************
      **Les �crans de l'application**  
      ******************************* 
       Screen section.

      **Ecran du menu principal 
        
       01 S-MENU-PRINCIPAL background-color is CouleurFond foreground-color is CouleurCara.
           05 line 1 col 1 blank Screen.
           05 line 1 col 64 value "DATE:".
           05 line 1 col 69 from Jour of DateSysteme.
           05 line 1 col 71 value "/".
           05 line 1 col 72 from Mois of DateSysteme.
           05 line 1 col 74 value "/".
           05 line 1 col 75 from Annee of DateSysteme.
           05 line 2 col 36 value "********".
           05 line 3 col 36 value "* EKIA *".
           05 line 4 col 1 PIC X(80) value all "*".
           05 line 5 col 32 value "Option choisie: ".
           05 line 6 col 1 PIC X(80) value all "*".
           05 line 11 col 28 value "1 - GESTION DES CLIENTS ....".
           05 line 13 col 28 value "2 - GESTION DES ARTICLES ...".
           05 line 15 col 28 value "3 - IMPORT D'UNE COMMANDE ..".
           05 line 19 col 27 value all "Les meubles aux noms ubuesques".  
           05 line 22 col 1 PIC X(80) value all "*".
           05 line 23 col 34 value all "0 - QUITTER". 
           05 line 24 col 1 PIC X(80) value all "*".      
           

       01 S-CADRE background-color is CouleurFond foreground-color is CouleurCara.
           05 line 1 col 1 blank Screen.
           05 line 1 col 2 value "EKIA".
           05 line 1 col 64 value "DATE:".
           05 line 1 col 69 from Jour of DateSysteme.
           05 line 1 col 71 value "/".
           05 line 1 col 72 from Mois of DateSysteme.
           05 line 1 col 74 value "/".
           05 line 1 col 75 from Annee of DateSysteme.
           05 line 2 col 23 PIC X(36) value all "*".
           05 line 3 col 23 value "*".
           05 line 3 col 24 from S-Titre.
           05 line 3 col 58 value "*".
           05 line 4 col 1 PIC X(80) value all "*".
           05 line 5 col 32 value all "Option choisie: ".
           05 line 5 col 48 using Option.
           05 line 6 col 1 PIC X(80) value all "*".
           05 line 22 col 1 PIC X(80) value all "*".
           05 line 23 col 34 value all "0 - QUITTER".
           05 line 24 col 1 PIC X(80) value all "*".      
           
       01 S-MENU-CLIENTS background-color is CouleurFond foreground-color is CouleurCara.
           05 line 11 col 28 value "1 - LISTE DES CLIENTS ......".
           05 line 13 col 28 value "2 - GESTION DE CLIENTS .....".
           
       01 S-MENU-ARTICLES background-color is CouleurFond foreground-color is CouleurCara.   
           05 line 11 col 28 value "1 - LISTE DES ARTICLES ......".
           05 line 13 col 28 value "2 - GESTION D'UN ARTICLE ....".
          
       01 S-FORMULAIRE-CLIENT background-color is CouleurFond foreground-color is CouleurCara.
           05 line 8 col 32 from S-SousTitre.
           05 line 10 col 15 value ".... Intitule : ".
           05 line 10 col 31 from INTITULE of CLIENT.
           05 line 11 col 15 value "...... Prenom : ".
           05 line 11 col 31 from PRENOM of CLIENT.
           05 line 12 col 15 value "......... Nom : ".
           05 line 12 col 31 from NOM of CLIENT.
           05 line 13 col 15 value "... Telephone : ".
           05 line 13 col 31 from TELEPHONE of CLIENT.
           05 line 14 col 15 value "........ Mail : ".
           05 line 14 col 31 from MAIL of CLIENT.
           05 line 15 col 15 value "..... Adresse : ".
           05 line 15 col 31 from ADRESSE of CLIENT.
           05 line 16 col 15 value ". Code Postal : ".
           05 line 16 col 31 from CODE_POSTAL of CLIENT.
           05 line 17 col 15 value "....... Ville : ".
           05 line 17 col 31 from VILLE of CLIENT.

       01 A-FORMULAIRE-CLIENT.
         05 line 9 col 32 value "Annuler (Y) : ".
         05 line 9 col 52 using Annuler.
         05 line 10 col 31 using INTITULE of CLIENT.
         05 line 11 col 31 using PRENOM of CLIENT.
         05 line 12 col 31 using NOM of CLIENT.
         05 line 13 col 31 PIC 9(10) using TELEPHONE of CLIENT.
         05 line 14 col 31 using MAIL of CLIENT.
         05 line 15 col 31 using ADRESSE of CLIENT.
         05 line 16 col 31 PIC 9(5) using CODE_POSTAL of CLIENT.
         05 line 17 col 31 using VILLE of CLIENT.

       01 S-MESSAGE-FORMULAIRE.
           05 line 18 col 10 from S-Message.

       01 S-MENU-FORMULAIRE-AJOUT.
         05 line 20 col 10 value "A - Ajouter ".
         
       01 S-MENU-FORMULAIRE-CHERCHER.
         05 line 19 col 10 value "C - Chercher".
       
       01 S-MENU-FORMULAIRE-ACTIONS.
         05 line 19 col 50 value "S - Supprimer".
         05 line 20 col 50 value "M - Modifier ".

       01 S-MESSAGE-REFUS-COMMANDE.
           05 line 21 col 15 from S-Message.

       01  S-LISTE-CLIENT-ENTETE background-color is CouleurFond foreground-color is CouleurCara. 
           05 line 7 col 1 value "N".
           05 line 7 COL 5 value "NOM".
           05 line 7 COL 22 value "PRENOM".
           05 line 7 COL 40 value "TELEPHONE".
           05 line 7 COL 52 value "CP".
           05 line 7 COL 59 value "VILLE".
           05 line 23 col 2 value "P/S - Naviguer".
           05 line 23 col 66 value "XX - Selection".

       01 S-LISTE-CLIENT-LIGNE background-color is CouleurFond foreground-color is CouleurCara.
           05 line NumeroLigne col 1 from LigneTableau pic 9(2).
           05 line NumeroLigne col 5 from Nom of CLIENT pic x(15).
           05 line NumeroLigne col 22 from Prenom of CLIENT pic x(15).
           05 line NumeroLigne col 40 from Telephone of CLIENT pic x(15).
           05 line NumeroLigne col 52 from CODE_POSTAL of CLIENT pic 9(5).
           05 line NumeroLigne col 59 from VILLE of CLIENT pic x(20).

       01 S-LISTE-ARTICLE-ENTETE background-color is CouleurFond foreground-color is CouleurCara.
           05 line 7 col 1 value "N".
           05 line 7 col 6 value "NOM".
           05 line 7 col 29 value "PRIX".
           05 line 7 col 46 value "STOCK".
           05 line 7 col 59 value "STOCK MIN".
           05 line 23 col 2 value "P/S - Naviguer".
           05 line 23 col 66 value "XX - Selection".

       01 S-LISTE-ARTICLE-LIGNE background-color is CouleurFond foreground-color is CouleurCara.
           05 line NumeroLigne col 1 from LigneTableau pic 9(2).    
           05 line NumeroLigne col 6 from Nom of ARTICLE pic x(20).    
           05 line NumeroLigne col 29 from PRIX of ARTICLE pic z(9)9v,99.    
           05 line NumeroLigne col 46 from STOCK of ARTICLE pic z(9)9.    
           05 line NumeroLigne col 59 from STOCK_MIN of ARTICLE pic z(9)9.

       01 S-FORMULAIRE-ARTICLE background-color is CouleurFond foreground-color is CouleurCara.
         05 line 8 col 32 from S-SousTitre.
         05 line 10 col 15 value "..... Reference : ".
         05 line 10 col 33 from ID_ARTICLE of ARTICLE.
         05 line 11 col 15 value "........... Nom : ".
         05 line 11 col 33 from NOM of ARTICLE.
         05 line 12 col 15 value ".......... Prix : ".
         05 line 12 col 33 from PRIX of ARTICLE pic z(9)9v,99.
         05 line 13 col 15 value "......... Stock : ".
         05 line 13 col 33 from STOCK of ARTICLE pic z(9)9.
         05 line 14 col 15 value ". Stock Minimum : ".
         05 line 14 col 33 from STOCK_MIN of ARTICLE pic z(9)9.


       01 S-FORMULAIRE-ARTICLE-RECHERCHE.
         05 line 12 col 50 value "+/-/= :".
         05 line 13 col 50 value "+/-/= :".
         05 line 14 col 50 value "+/-/= :".
         05 line 15 col 18 value "Stock < Stock Min (Y) : ".
         05 line 16 col 18 value "Stock > Stock Min (Y) : ".

       01 A-FORMULAIRE-ARTICLE.
         05 line 9 col 32 value "Annuler (Y) : ".
         05 line 9 col 52 using Annuler.
         05 line 11 col 33 using NOM of ARTICLE.
         05 line 12 col 33 using PRIX of ARTICLE pic z(9)9v,99.
         05 line 13 col 33 using STOCK of ARTICLE pic z(9)9.
         05 line 14 col 33 using STOCK_MIN of ARTICLE pic z(9)9.

       01 A-FORMULAIRE-ARTICLE-RECHERCHE.
         05 line 9 col 32 value "Annuler (Y) : ".
         05 line 9 col 52 using Annuler.
         05 line 11 col 33 using NOM of ARTICLE.
         05 line 12 col 33 using PRIX of ARTICLE pic z(9)9v,99.
         05 line 12 col 56 using TEST_PRIX pic X.
         05 line 13 col 33 using STOCK of ARTICLE pic z(9)9.
         05 line 13 col 56 using TEST_STOCK pic X.
         05 line 14 col 33 using STOCK_MIN of ARTICLE pic z(9)9.
         05 line 14 col 56 using TEST_STOCK_MIN pic X.
         05 line 15 col 42 using TEST_STOCK_INF_MIN pic X.
         05 line 16 col 42 using TEST_STOCK_SUP_MIN pic X.

       01 S-COMMANDE background-color is CouleurFond foreground-color is CouleurCara.
           05 line 7 col 4 value "NOM: " .
           05 line 7 col 9 from NOM of CLIENT pic x(20).
           05 line 7 col 33 value "PRENOM: ".
           05 line 7 col 41 from PRENOM of CLIENT PIC X(12).
           05 line 8 col 4 value "MAIL: ".       
           05 line 8 col 10 from MAIL of CLIENT PIC X(21).
           05 line 8 col 33 value "VILLE: ".  
           05 line 8 col 40 from VILLE of CLIENT PIC X(18).
           05 line 8 col 60 value "CP: ".
           05 line 8 col 64 from CODE_POSTAL of CLIENT PIC 9(5).
           05 line 9 col 4 value "TELEPHONE: ".   
           05 line 9 col 15 from TELEPHONE of CLIENT PIC 9(10).
           05 line 9 col 33 value "ADRESSE: ".       
           05 line 9 col 42 from ADRESSE of CLIENT PIC X(37).
           05 line 10 col 1 PIC X(80) value all "*".
           05 line 11 col 5 value "CONTENU COMMANDE ".           
           05 line 11 col 29 value "QUANTITE ".           
           05 line 11 col 49 value "STOCK ".           
           05 line 11 col 67 value "PRIX ".           
           05 line 20 col 1 PIC X(80) value all "*".
           05 line 21 col 10 value "1 - ACCUSE DE RECEPTION".
           05 line 21 col 50 value "2 - FACTURE".
           05 line 23 col 2 value "P/S - Naviguer".


       01 S-COMMANDE-ACCUSE-FINI.
         05 line 21 col 10 value "GENERATION ACCUSE REUSSIE".

       01 S-COMMANDE-FACTURE-FINI.
         05 line 21 col 50 value "GENERATION FACTURE REUSSIE".

       01 F-COMMANDE-LIGNE-ARTICLE background-color is CouleurFond foreground-color is CouleurCara.
           05 line NumeroLigne col 4 from NOM of ARTICLE pic X(21).
           05 line NumeroLigne col 32 from QUANTITE of COMMANDE pic z(3)9.
           05 line NumeroLigne col 50 from STOCK of ARTICLE pic z(3)9.
           05 line NumeroLigne col 67 from PRIX of COMMANDE pic z(9)9v,99.

       

       procedure division.
           perform MENU.
      * Menu principal de l'application
       MENU.
           perform MENU-INIT.
           perform MENU-TRT until Option = '0 '.
           perform MENU-END.

       MENU-INIT.

           move 'XX' to Option.
           accept DateSysteme from date.

      ******** Connexion � la base de donn�es :

           perform INIT-CONNEXION.     
      
       MENU-TRT.
           move '0 ' to Option.
           display S-Menu-Principal.
           accept Option line 5 col 48.
                  
           evaluate Option
               when 1
                   perform MENU-CLIENT
               when 2
                   perform MENU-ARTICLE
               when 3
                   perform IMPORT-FICHIER    
           end-evaluate.
       MENU-END.

           stop run.

      ***********************************************
      *******           PARTIE CLIENT        ********
      ***********************************************
       MENU-CLIENT.

           perform MENU-CLIENT-INIT.
           perform MENU-CLIENT-TRT until Option = '0 '.
           perform MENU-CLIENT-END.

       MENU-CLIENT-INIT.

           move 1 to Option.
           move space to CLIENT.
           accept DateSysteme from date.

       MENU-CLIENT-TRT.
           move "          GESTION CLIENT" to S-Titre.
           move '0 ' to Option.
           display S-CADRE.
           display S-MENU-CLIENTS.
           accept S-CADRE.
    
           evaluate Option
               when '1 '
                   move 1 to SQL-newIndex-cursor
                   perform LISTE-CLIENT
               when '2 '
                   move space to CLIENT
                   perform FORMULAIRE-CLIENT
           end-evaluate.  
        
       MENU-CLIENT-END.
           move 'XX' to Option.          
      
               
              
                            
      ******** PARTIE LISTE
      
       LISTE-CLIENT.      
           
           perform LISTE-CLIENT-INIT.
           perform LISTE-CLIENT-TRT UNTIL EOT = 1.
           perform LISTE-CLIENT-END.
       
       LISTE-CLIENT-INIT.
       
           move 0 to EOT.

      * Appel du curseur    
           perform INIT-CURSOR-CLIENT.

      * Initialisation de la pagination / Ent�te de la screen
           move '0 ' to Option.
           display S-CADRE.
           display S-LISTE-CLIENT-ENTETE.

      * Initialisation des variables de lignes
           move 7 to NumeroLigne.
           move 0 to LigneTableau.
           move 1 to SQL-newIndex-cursor.
           set SQL-update-cursor-TRUE to TRUE.
             
       LISTE-CLIENT-TRT.
           display S-LISTE-CLIENT-ENTETE.

      *    Si on doit changer de position dans la liste lors d'une nouvelle page
           if SQL-update-cursor-TRUE then
               move SQL-newIndex-cursor to SQL-index-cursor
               perform FETCH-CURSOR-ABSOLUTE-CLIENT
               set SQL-update-cursor-FALSE to TRUE

      *    Suite de la creation des lignes jusqu'a arriver � la fin du tableau
           else if NumeroLigne <> 21 then
               perform FETCH-CURSOR-CLIENT
           end-if.
           
      *    Si on n'arrive pas a la fin de la table et que le client n'est pas vide
           if SQLCODE <> 100 then
               perform AFFICHAGE-LISTE-CLIENT
               if LigneTableau <> 0 AND Nom of CLIENT <> space then
                   move CLIENT to TABLEAU(LigneTableau)
               end-if
               move space to CLIENT               
           else
               perform ACCEPT-FIN-LISTE-CLIENT
           end-if.
           
       LISTE-CLIENT-END.
           perform CLOSE-CURSOR-CLIENT.

      *    Reinitialisation des ecrans et valeurs    
           perform varying NumeroLigne from 1 by 1 until NumeroLigne > 13
               move space to TABLEAU(NumeroLigne)
           end-perform.
           move '0 ' to Option.
           move space to CLIENT.
           display S-CADRE.
           move 'XX' to Option.


       AFFICHAGE-LISTE-CLIENT.  
           if NumeroLigne <> 21 then
               if Nom of CLIENT <> space then
                   add 1 to NumeroLigne
                   add 1 to LigneTableau
                   display S-LISTE-CLIENT-LIGNE
               end-if
           else
               perform ACCEPT-FIN-LISTE-CLIENT
           end-if.            
       
       ACCEPT-FIN-LISTE-CLIENT.
           accept S-CADRE
           move Option to OptionNum.

      *    Si l'utilisateur veut quitter l ecran    
           if Option = '00' or Option ='0 'or Option = ' 0' then
               move 1 to EOT

      *    Si l'utilisatuer veut editer une des lignes        
           else if 0 < OptionNum AND OptionNum <= LigneTableau then
               move TABLEAU(OptionNum) to CLIENT
               perform FORMULAIRE-CLIENT
               perform REINIT-LISTE-CLIENT
              
      *    Si l'utilisateur veut passer a la page suivante
           else if (option = 's ' or option = 'S ' )

      *        On ne peut pas passer a la page suivante si on est a la fin de la table    
               if SQLCODE <> 100 then 
                   perform REINIT-LISTE-CLIENT
                   add 14 to SQL-newIndex-cursor
               end-if

      *    Si l'utilisateur veut retourner a la page precedente
           else if (option = 'p ' or option = 'P ') 

      *        Si on peut encore reculer de 14 lignes
               if (SQL-newIndex-cursor > 14) then
                   perform REINIT-LISTE-CLIENT
                   subtract 14 from SQL-newIndex-cursor

      *        Si on ne peut plus reculer de 14 lignes on remet le curseur en position 1            
               else if SQL-newIndex-cursor <> 1 then
                   perform REINIT-LISTE-CLIENT
                   move 1 to SQL-newIndex-cursor

               end-if

           end-if.

       REINIT-LISTE-CLIENT.
           move 7 to NumeroLigne.
           move 0 to LigneTableau.
           display S-CADRE.
           set SQL-update-cursor-TRUE to true.

      ******** PARTIE FORMULAIRE
       FORMULAIRE-CLIENT.
           perform FORMULAIRE-CLIENT-INIT.
           perform FORMULAIRE-CLIENT-TRT until option = "0 ".
           perform FORMULAIRE-CLIENT-END.

       FORMULAIRE-CLIENT-INIT.
           move '0 ' to Option.
           display S-CADRE.
           move "Gestion d'un client" to S-SousTitre.
           display S-FORMULAIRE-CLIENT.
           move 'XX' to Option.

       FORMULAIRE-CLIENT-TRT.
      *    Affichage du menu avec les options disponibles
      *    Si on arrive depuis une liste
           if tableau(1) = space then
               display S-MENU-FORMULAIRE-AJOUT
               display S-MENU-FORMULAIRE-CHERCHER
           end-if.
      
      *    Si un client a ete selectionne soit depuis une liste soit depuis une recherche
           if client <> space then
               display S-MENU-FORMULAIRE-ACTIONS
           end-if.

           move '0 ' to Option.
           accept S-CADRE.
           move space to S-Message.
           display S-MESSAGE-FORMULAIRE.
           evaluate Option
               when 'S '
               when 's '
                   if CLIENT <> space then
                       perform FORMULAIRE-CLIENT-SUPPRESSION
                   end-if
               when 'M '
               when 'm '
                   if CLIENT <> space then 
                       perform FORMULAIRE-CLIENT-MODIFICATION
                   end-if
               when 'A '
               when 'a '
                   move space to CLIENT
                   perform FORMULAIRE-CLIENT-AJOUTER
               when 'C '
               when 'c '
                   perform FORMULAIRE-CLIENT-RECHERCHE
           end-evaluate.


       FORMULAIRE-CLIENT-END.
           move 'XX' to Option.
           if (TABLEAU(1) <> space ) then
               move '0 ' to Option
           end-if.
           perform REINIT-LISTE-CLIENT
           move space to CLIENT.

       FORMULAIRE-CLIENT-SUPPRESSION.
           move "Suppression du client en cours..." to S-Message.
           display S-MESSAGE-FORMULAIRE.

           perform DELETE-CLIENT.
           if SQLCODE = 0 OR SQLCODE = 1 then
               move "REUSSITE : Le client a ete supprime." to S-Message
               move space to CLIENT
           else
               move "ECHEC : Le client n'a pas ete supprime." to S-Message
           end-if.

           display S-MESSAGE-FORMULAIRE.

      *    Si on est arrive depuis une liste on y retourne   
           if TABLEAU(1) <> space then
               move '0 ' to Option
           end-if.



       FORMULAIRE-CLIENT-AJOUTER.
           move 0 to EstValide.
           move space to Annuler.
           display A-FORMULAIRE-CLIENT.
      
      *    Saisie des information du client
           perform until EstValide = 1 or Annuler = 'Y' or Annuler = 'y'
               display S-FORMULAIRE-CLIENT
               accept A-FORMULAIRE-CLIENT
               if Annuler <> 'Y' or Annuler = 'y'then
                   move 1 to EstValide
                   perform TEST-CLIENT-VALIDE
               end-if
           end-perform.

           if EstValide = 1
               move "Ajout du client en cours..." to S-Message
               display S-MESSAGE-FORMULAIRE
               perform INSERT-CLIENT

               if SQLCODE = 0 OR SQLCODE = 1 then
                   move "REUSSITE : Le client a ete ajoute." to S-Message
               else
                   move "ECHEC : Le client n'a pas ete ajoute." to S-Message
               end-if
               display S-MESSAGE-FORMULAIRE
           else 
               move space to CLIENT
           end-if.
       
       FORMULAIRE-CLIENT-RECHERCHE.
       
           move space to Annuler.
           move space to CLIENT.
           display A-FORMULAIRE-CLIENT.
           display S-FORMULAIRE-CLIENT.
           accept A-FORMULAIRE-CLIENT.
           if Annuler <> "Y" and Annuler <> "y" then
               move "Recherche du client en cours... " to S-Message
               display S-MESSAGE-FORMULAIRE

               perform COUNT-CLIENT
               if SQLCODE = 0 or SQLCODE = 1 then
                   if NombreClient = 0 then
                       move "INFO : Aucun client ne correspond aux donnees renseignees." to S-Message
      *                On remet a zero le client    
                       move space to CLIENT
                   else if NombreClient = 1 then
                       move "INFO : Un client correspondant a ete trouve." to S-Message
      *                On recupere les donnees du client trouve            
                       perform SELECT-CLIENT-INCLUSIF
                   else 
                       move "INFO : Plus d'un client avec ses infos ont �t� trouve." to S-Message
      *                On affiche une liste avec les clients trouves            
                       perform LISTE-CLIENT
                   end-if
               else
                   move space to CLIENT
                   move "ECHEC : La requete n'a pas abouti." to S-Message
               end-if
               display S-MESSAGE-FORMULAIRE

      *        Rafraichissement de l'ecran
               display S-FORMULAIRE-CLIENT
           end-if.
           move space to Annuler.
       
       FORMULAIRE-CLIENT-MODIFICATION.
           move 0 to EstValide.
           move space to Annuler.
           display A-FORMULAIRE-CLIENT.
           move CLIENT to TABLEAU(1).

           perform until EstValide = 1 or Annuler = 'Y' or Annuler = 'y'
               if Annuler <> 'Y' and Annuler <> 'y' then 
                   display S-FORMULAIRE-CLIENT
                   accept A-FORMULAIRE-CLIENT
                   move 1 to EstValide
                   perform TEST-CLIENT-VALIDE
               else 
                   move 0 to EstValide
                   move TABLEAU(1) to CLIENT
               end-if
           end-perform.
           
           if EstValide = 1 then
               move "Mise a jour du client en cours." to S-Message
               display S-MESSAGE-FORMULAIRE
               perform UPDATE-CLIENT

               if SQLCODE = 0 OR SQLCODE = 1 then
                   move "REUSSITE : Le client a ete mis a jour." to S-Message
               else
                   move "ECHEC : Le client n'a pas ete mis a jour." to S-Message
               end-if
               display S-MESSAGE-FORMULAIRE
           end-if.
       
       TEST-CLIENT-VALIDE.
           if INTITULE of CLIENT = space then
               move 0 to EstValide
               move "ERREUR : Intitule non rempli." to S-Message
           end-if.

           if NOM of CLIENT = space then 
               move 0 to EstValide
               move "ERREUR : Nom non rempli." to S-Message
           end-if.
           
           if PRENOM of CLIENT = space then
               move 0 to EstValide
               move "ERREUR : Prenom non rempli." to S-Message
           end-if.
           
           if TELEPHONE of CLIENT = space then
               move 0 to EstValide
               move "ERREUR : Telephone non rempli." to S-Message
           end-if.
           
           if ADRESSE of CLIENT = space then
               move 0 to EstValide
               move "ERREUR : Adresse non remplie." to S-Message
           end-if.
           
           if CODE_POSTAL of CLIENT = space then
               move 0 to EstValide
               move "ERREUR : Code Postal non rempli." to S-Message
           end-if.
           
           if VILLE of CLIENT = space then
               move 0 to EstValide
               move "ERREUR : Ville non remplie." to S-Message
           end-if.

           display S-MESSAGE-FORMULAIRE.
           

      ************************************************
      *******         PARTIE ARTICLE         ********
      ***********************************************
       MENU-ARTICLE.

           perform MENU-ARTICLE-INIT.
           perform MENU-ARTICLE-TRT until Option = '0 '.
           perform MENU-ARTICLE-END.

       MENU-ARTICLE-INIT.

           move 'XX' to Option.
           accept DateSysteme from date.

       MENU-ARTICLE-TRT.

           move "          GESTION ARTICLE" to S-Titre.
           move '0 ' to Option.
           display S-CADRE.
           display S-MENU-ARTICLES.
           accept S-CADRE.

           evaluate Option
               when '1 ' 
                   perform LISTE-ARTICLE
               when '2 '
                   perform FORMULAIRE-ARTICLE
           end-evaluate.               

       MENU-ARTICLE-END.
           
           move 'XX' to Option.
                          
      ******** PARTIE LISTE
      
       LISTE-ARTICLE.      
           
           perform LISTE-ARTICLE-INIT.
           perform LISTE-ARTICLE-TRT UNTIL EOT = 1.
           perform LISTE-ARTICLE-END.
       
       LISTE-ARTICLE-INIT.
       
           move 0 to EOT.

      * Appel du curseur    
           perform INIT-CURSOR-ARTICLE.
      * Initialisation de la pagination / Ent�te de la screen
           move '0 ' to Option.
           display S-CADRE.
           display S-LISTE-ARTICLE-ENTETE.

      * Initialisation des variables de lignes
           move 7 to NumeroLigne.
           move 0 to LigneTableau.
           move 1 to SQL-newIndex-cursor.
           set SQL-update-cursor-TRUE to TRUE.
             
       LISTE-ARTICLE-TRT.
           display S-LISTE-ARTICLE-ENTETE.

      *    Si on doit changer de position dans la liste lors d'une nouvelle page
           if SQL-update-cursor-TRUE then
               move SQL-newIndex-cursor to SQL-index-cursor
               perform FETCH-CURSOR-ABSOLUTE-ARTICLE
               set SQL-update-cursor-FALSE to TRUE

      *    Suite de la creation des lignes jusqu'a arriver � la fin du tableau
           else if NumeroLigne <> 21 then
               perform FETCH-CURSOR-ARTICLE
           end-if.
      *    Si on n'arrive pas a la fin de la table et que le ARTICLE n'est pas vide
           if SQLCODE <> 100 then
               perform AFFICHAGE-LISTE-ARTICLE
               if LigneTableau <> 0 and Nom of ARTICLE <> space then
                   move ARTICLE to TABLEAU(LigneTableau)
               end-if
               move space to ARTICLE
           else
               perform ACCEPT-FIN-LISTE-ARTICLE
           end-if.
           
       LISTE-ARTICLE-END.
           perform CLOSE-CURSOR-ARTICLE.

      *    Reinitialisation des ecrans et valeurs    
           perform varying NumeroLigne from 1 by 1 until NumeroLigne > 13
               move space to TABLEAU(NumeroLigne)
           end-perform.
           move '0 ' to Option.
           move space to ARTICLE.
           display S-CADRE.
           move 'XX' to Option.

       AFFICHAGE-LISTE-ARTICLE.  
           if NumeroLigne <> 21 then
               if Nom of ARTICLE <> space then
                   add 1 to NumeroLigne
                   add 1 to LigneTableau
                   display S-LISTE-ARTICLE-LIGNE
               end-if
           else
               perform ACCEPT-FIN-LISTE-ARTICLE
           end-if.            
       
       ACCEPT-FIN-LISTE-ARTICLE.
           move '0 ' to Option.
           accept S-CADRE.
           move Option to OptionNum.

      *    Si l'utilisateur veut quitter l ecran    
           if Option = '00' or Option ='0 'or Option = ' 0' then
               move 1 to EOT

      *    Si l'utilisatuer veut editer une des lignes        
           else if 0 < OptionNum AND OptionNum <= LigneTableau then
               move TABLEAU(OptionNum) to ARTICLE
               perform FORMULAIRE-ARTICLE
               perform REINIT-LISTE-ARTICLE
              
      *    Si l'utilisateur veut passer a la page suivante
           else if (option = 's ' or option = 'S ' )

      *        On ne peut pas passer a la page suivante si on est a la fin de la table    
               if SQLCODE <> 100 then 
                   perform REINIT-LISTE-ARTICLE
                   add 14 to SQL-newIndex-cursor
               end-if

      *    Si l'utilisateur veut retourner a la page precedente
           else if (option = 'p ' or option = 'P ') 

      *        Si on peut encore reculer de 14 lignes
               if (SQL-newIndex-cursor > 14) then
                   perform REINIT-LISTE-ARTICLE
                   subtract 14 from SQL-newIndex-cursor

      *        Si on ne peut plus reculer de 14 lignes on remet le curseur en position 1            
               else if SQL-newIndex-cursor <> 1 then
                   perform REINIT-LISTE-ARTICLE
                   move 1 to SQL-newIndex-cursor

               end-if

           end-if.

       REINIT-LISTE-ARTICLE.
           move 7 to NumeroLigne.
           move 0 to LigneTableau.
           display S-CADRE.
           set SQL-update-cursor-TRUE to true.

      ******** PARTIE FORMULAIRE
       FORMULAIRE-ARTICLE.
           perform FORMULAIRE-ARTICLE-INIT.
           perform FORMULAIRE-ARTICLE-TRT until option = "0 ".
           perform FORMULAIRE-ARTICLE-END.

       FORMULAIRE-ARTICLE-INIT.
           move '0 ' to Option.
           display S-CADRE.
           move "Gestion d'un ARTICLE" to S-SousTitre.
           display S-FORMULAIRE-ARTICLE.
           move 'XX' to Option.

       FORMULAIRE-ARTICLE-TRT.
           move '0 ' to Option.
           display S-CADRE.
           display S-FORMULAIRE-ARTICLE.
           display S-MESSAGE-FORMULAIRE.
           move space to S-Message.
      *    Affichage du menu avec les options disponibles
      *    Si on arrive depuis une liste
           if tableau(1) = space then
               display S-MENU-FORMULAIRE-AJOUT
               display S-MENU-FORMULAIRE-CHERCHER
           end-if.
      
      *    Si un ARTICLE a ete selectionne soit depuis une liste soit depuis une recherche
           if ARTICLE <> space then
               display S-MENU-FORMULAIRE-ACTIONS
           end-if.

           accept S-CADRE.
           display S-MESSAGE-FORMULAIRE.
           evaluate Option
               when 'S '
               when 's '
                   if ARTICLE <> space then
                       perform FORMULAIRE-ARTICLE-SUPPRESSION
                   end-if
               when 'M '
               when 'm '
                   if ARTICLE <> space then 
                       perform FORMULAIRE-ARTICLE-MODIFICATION
                   end-if
               when 'A '
               when 'a '
                   if tableau(1) = space then
                       move space to ARTICLE
                       perform FORMULAIRE-ARTICLE-AJOUTER
                   end-if
               when 'C '
               when 'c '
                   if tableau(1) = space then
                       perform FORMULAIRE-ARTICLE-RECHERCHE
                   end-if
           end-evaluate.


       FORMULAIRE-ARTICLE-END.
           move '0 ' to Option.
           perform REINIT-LISTE-ARTICLE
           move space to ARTICLE.

       FORMULAIRE-ARTICLE-SUPPRESSION.
           move "Suppression du ARTICLE en cours..." to S-Message.
           display S-MESSAGE-FORMULAIRE.

           perform DELETE-ARTICLE.

           if SQLCODE = 0 OR SQLCODE = 1 then
               move "REUSSITE : L'ARTICLE a ete supprime." to S-Message
               move space to ARTICLE
           else
               move "ECHEC : L'ARTICLE n'a pas ete supprime." to S-Message
           end-if.

           display S-MESSAGE-FORMULAIRE.

      *    Si on est arrive depuis une liste on y retourne   
           if TABLEAU(1) <> space then
               move '0 ' to Option
           end-if.


       FORMULAIRE-ARTICLE-AJOUTER.
           move 0 to EstValide.
           move space to Annuler.
           display A-FORMULAIRE-ARTICLE.
      
      *    Saisie des information du ARTICLE
           perform until EstValide = 1 or Annuler = 'Y' or Annuler = 'y'
               display S-FORMULAIRE-ARTICLE
               accept A-FORMULAIRE-ARTICLE
               if Annuler <> 'Y' and Annuler <> 'y' then
                   move 1 to EstValide
                   perform TEST-ARTICLE-VALIDE
               end-if
           end-perform.

           if EstValide = 1
               move "Ajout du ARTICLE en cours..." to S-Message
               display S-MESSAGE-FORMULAIRE
               perform INSERT-ARTICLE

               if SQLCODE = 0 OR SQLCODE = 1 then
                   move "REUSSITE : L'ARTICLE a ete ajoute." to S-Message
               else
                   move "ECHEC : L'ARTICLE n'a pas ete ajoute." to S-Message
               end-if
               display S-MESSAGE-FORMULAIRE
           else 
               move space to ARTICLE
           end-if.
       
       FORMULAIRE-ARTICLE-RECHERCHE.
           move space to Annuler.
           move space to ARTICLE.
           display S-FORMULAIRE-ARTICLE.
           display A-FORMULAIRE-ARTICLE.
           display S-FORMULAIRE-ARTICLE-RECHERCHE.
           accept A-FORMULAIRE-ARTICLE-RECHERCHE.

           if Annuler <> 'y' and Annuler <> 'Y' then
               move "Recherche du ARTICLE en cours... " to S-Message
               display S-MESSAGE-FORMULAIRE

               perform COUNT-ARTICLE
               if SQLCODE = 0 or SQLCODE = 1 then
                   if NombreARTICLE = 0 then
                       move "INFO : Aucun ARTICLE ne correspond aux donnees renseignees." to S-Message
      *                On remet a zero le ARTICLE    
                       move space to ARTICLE
                   else if NombreARTICLE = 1 then
                       move "INFO : Un ARTICLE correspondant a ete trouve." to S-Message
      *                On recupere les donnees du ARTICLE trouve            
                       perform SELECT-ARTICLE-INCLUSIF
                   else 
                       move "INFO : Plus d'un ARTICLE avec ses infos ont �t� trouve." to S-Message
      *                On affiche une liste avec les ARTICLEs trouves            
                       perform LISTE-ARTICLE
                   end-if
               else
                   move space to ARTICLE
                   move "ECHEC : La requete n'a pas abouti." to S-Message
               end-if
               display S-MESSAGE-FORMULAIRE

      *        Rafraichissement de l'ecran
               display S-FORMULAIRE-ARTICLE
           end-if.
           move space to Annuler.

       
       FORMULAIRE-ARTICLE-MODIFICATION.
           move 0 to EstValide.
           move space to Annuler.
           display A-FORMULAIRE-ARTICLE.
           move ARTICLE to TABLEAU(1).

           perform until EstValide = 1 or Annuler = 'Y' or Annuler = 'y'
               if Annuler <> 'Y' and Annuler <> 'y' then 
                   display S-FORMULAIRE-ARTICLE
                   accept A-FORMULAIRE-ARTICLE
                   move 1 to EstValide
                   perform TEST-ARTICLE-VALIDE
               else 
                   move 0 to EstValide
                   move TABLEAU(1) to ARTICLE
               end-if
           end-perform.
           move TABLEAU(2) to TABLEAU(1)

           if EstValide = 1 then
               move "Mise a jour du ARTICLE en cours." to S-Message
               display S-MESSAGE-FORMULAIRE
               perform UPDATE-ARTICLE

               if SQLCODE = 0 OR SQLCODE = 1 then
                   move "REUSSITE : L'ARTICLE a ete mis a jour." to S-Message
               else
                   move "ECHEC : L'ARTICLE n'a pas ete mis a jour." to S-Message
               end-if
               display S-MESSAGE-FORMULAIRE
           end-if.
       
       TEST-ARTICLE-VALIDE.
           if NOM of ARTICLE = space then 
               move 0 to EstValide
               move "ERREUR : Nom non rempli." to S-Message
           end-if.
           
           if PRIX of ARTICLE = 0 then
               move 0 to EstValide
               move "ERREUR : Un article ne peut pas etre gratuit." to S-Message
           end-if.
           
           if STOCK_MIN of ARTICLE = 0 then
               move 0 to EstValide
               move "ERREUR : Le stock minimum ne peut etre de 0." to S-Message
           end-if.

           display S-MESSAGE-FORMULAIRE.
           

      ***********************************************
      *******         PARTIE COMMANDE        ********
      ***********************************************

      **IMPORTATION DU FICHIER
      
       IMPORT-FICHIER.
           perform IMPORT-FICHIER-INIT.
           perform IMPORT-FICHIER-TRT until option = '0 '.
           perform IMPORT-FICHIER-END.

       IMPORT-FICHIER-INIT.
           move "         IMPORT COMMANDE" to S-Titre.
           move space to S-Message.
           perform INIT-LIGNE-COMMANDE.
           perform AFFICHAGE-COMMANDE-INIT.
           perform AFFICHAGE-COMMANDE.
           move 'XX' to Option.

       IMPORT-FICHIER-TRT.
           perform AFFICHAGE-COMMANDE
           move '0 ' to Option
           accept S-CADRE.

           move 1 to NbLigne.
           move 1 to NumeroPage.

           evaluate Option
               when '1 '
                   perform AFFICHAGE-COMMANDE-END
                   perform IMPRESSION-ACCUSE-RECEPTION
                   perform AFFICHAGE-COMMANDE-INIT
                   display S-COMMANDE-ACCUSE-FINI

               when '2 '
                   perform AFFICHAGE-COMMANDE-END
                   perform IMPRESSION-FACTURE
                   perform AFFICHAGE-COMMANDE-INIT
                   display S-COMMANDE-FACTURE-FINI

               when 's '
               when 'S '
                   continue
                   if ARTICLE <> space then
                       move 0 to EOF
                       move 11 to NumeroLigne
                       display S-CADRE
                       display S-COMMANDE
                       perform AFFICHAGE-COMMANDE
                   end-if
               when 'p '
               when 'P '
                   perform AFFICHAGE-COMMANDE-END
                   perform AFFICHAGE-COMMANDE-INIT
           end-evaluate.

       IMPORT-FICHIER-END.
           move 'XX' to Option.
           perform AFFICHAGE-COMMANDE-END.
           move space to CLIENT.

       AFFICHAGE-COMMANDE.
           perform AFFICHAGE-COMMANDE-TRT UNTIL EOF = 1.

       AFFICHAGE-COMMANDE-INIT.
           move 0 to EOF.
           
           display S-CADRE.
           display S-COMMANDE.
           if S-Message <> space then
               display S-MESSAGE-REFUS-COMMANDE
               move space to S-Message
           end-if.


      * On ouvre le fichier en lecture (input) 
           open input F-Commande.  
      * On a pas besoin de lire la ligne titre, alors on passe directement � la 2�me ligne
           read F-Commande 
               at end move 1 to EOF
           end-read.
         
      * Affichage de la commande re�ue
           move 11 to NumeroLigne.
           
       AFFICHAGE-COMMANDE-TRT.
      *On lit l'enregistrement 
     
           read F-Commande 
               at end
                   move 1 to EOF
                   move space to ARTICLE
               not at end
                   perform AFFICHAGE-COMMANDE-LIGNE
           end-read.
                      
       AFFICHAGE-COMMANDE-LIGNE.  
           perform EXTRACTION-LIGNE-COMMANDE.
           if S-Message <> space then 
               move 1 to EOF
           end-if.

           if NumeroLigne <> 19 then
               add 1 to NumeroLigne
               display F-COMMANDE-LIGNE-ARTICLE
           else 
               move 1 to EOF
           end-if.    

       AFFICHAGE-COMMANDE-END.
           close F-Commande.

       INIT-LIGNE-COMMANDE.
           open input F-Commande.
           read F-Commande.
           read F-Commande.
           perform EXTRACTION-LIGNE-COMMANDE.
           close F-Commande.

      **Recherche du client existant ou non.
           move space to ID_CLIENT of CLIENT.
           perform SELECT-CLIENT-EXCLUSIF.
      **Si le client n'existe pas on le rajoute.
           if SQLCODE <> 0 and SQLCODE <> 1 then
               perform INSERT-CLIENT
           end-if.
           perform SELECT-CLIENT-PRENOM-MON.


       EXTRACTION-LIGNE-COMMANDE.
      **Eclatement de la chaine du fichier CSV gr�ce au signe ",".
           unstring E-Commande delimited by ";" into
             NOM of CLIENT
             PRENOM of CLIENT
             INTITULE of CLIENT
             TELEPHONE of CLIENT
             MAIL of CLIENT
             ADRESSE of CLIENT
             CODE_POSTAL of CLIENT
             VILLE of CLIENT
             ID_ARTICLE of ARTICLE
             LastZone
           end-unstring.
           unstring LastZone delimited by space into QUANTITE of COMMANDE.

           perform SELECT-ARTICLE-BY-ID.
      ** Si l'article n'existe pas.
           if SQLCODE = 100 then
               move "Article(s) non existant - Commande refusee" to S-Message
           end-if.

           multiply PRIX of ARTICLE by QUANTITE of COMMANDE giving PRIX OF COMMANDE.

      ************************************     
      *********** PARTIE ACCUSE-RECEPTION*
      ************************************
      ********** Impression paragraphe d�but jusqu'au d�but de la liste articles.    
       IMPRESSION-ACCUSE-RECEPTION.
           open input F-Reception-Entree.
           open output F-Reception-Sortie.

           perform IMPRESSION-ACCUSE-RECEPTION-PARAGRAPHE.
           perform IMPRESSION-ACCUSE-RECEPTION-LIGNE-ARTICLE.
           perform IMPRESSION-ACCUSE-RECEPTION-PARAGRAPHE.
           perform IMPRESSION-FIN-ACCUSE-RECEPTION.

           close F-Reception-Entree.
           close F-Reception-Sortie.

       IMPRESSION-ACCUSE-RECEPTION-PARAGRAPHE.
           perform IMPRESSION-ACCUSE-RECEPTION-PARAGRAPHE-INIT.
           perform IMPRESSION-ACCUSE-RECEPTION-PARAGRAPHE-TRT until EOF = 1.
           perform IMPRESSION-ACCUSE-RECEPTION-PARAGRAPHE-END.
           
       IMPRESSION-ACCUSE-RECEPTION-PARAGRAPHE-INIT.
           move 0 to EOF.

      **On lit le fichier F-Reception-Entree qui servira de template au fichier de sortie.     
       IMPRESSION-ACCUSE-RECEPTION-PARAGRAPHE-TRT.
           read F-Reception-Entree
               at end
                   move 1 to EOF
               not at end
                   perform REPLACE-ACCUSE-RECEPTION
           end-read.

      **On remplace les valeurs $ dans le fichier txt par les donn�es recup�rer dans la bdd. 
       REPLACE-ACCUSE-RECEPTION.
           inspect E-Reception-Entree tallying EstLigneArticle for all R-Liste-Article.
           if EstLigneArticle <> 0 then
               move 1 to EOF
               move 0 to EstLigneArticle
           else
               Inspect E-Reception-Entree replacing all R-NomClient by NomPrenom,
                 R-Adresse by Adresse,
                 R-CodePostal by CODE_POSTAL,
                 R-Ville by Ville,
                 R-ID-Client by ID_CLIENT of CLIENT,
                 R-ID-Commande by ID_COMMANDE,
                 R-Jour by Jour of DateSysteme,
                 R-Mois by Mois of DateSysteme,
                 R-Annee by Annee of DateSysteme,
                 R-Date-Commande by DATE_COMMANDE

               write E-Reception-Sortie from E-Reception-Entree
               add 1 to NbLigne

           end-if.

       IMPRESSION-ACCUSE-RECEPTION-PARAGRAPHE-END.
           move 0 to EOF.

       IMPRESSION-ACCUSE-RECEPTION-LIGNE-ARTICLE.
           perform IMPRESSION-ACCUSE-RECEPTION-LIGNE-ARTICLE-INIT.
           perform IMPRESSION-ACCUSE-RECEPTION-LIGNE-ARTICLE-TRT until EOF = 1.
           perform IMPRESSION-ACCUSE-RECEPTION-LIGNE-ARTICLE-END.

      **on ouvre le fichier csv commande pour recup�rer les ligne de la commande.     
       IMPRESSION-ACCUSE-RECEPTION-LIGNE-ARTICLE-INIT.
           open input F-Commande.
           move 0 to EOF.

           write E-Reception-Sortie from LigneEtoiles.
           write E-Reception-Sortie from LigneEntete.
           write E-Reception-Sortie from LigneEtoiles.
           add 3 to NbLigne.

           read F-Commande
               at end
                   move 1 to EOF
           end-read.

      **On lit le fichier csv de commande et on �clate la ligne pour r�cuperer les donn�es
      **puis on move le tout dans les chamos correspondants.
      **et on �crit les lignes.     
       IMPRESSION-ACCUSE-RECEPTION-LIGNE-ARTICLE-TRT.
           read F-Commande
               at end 
                   move 1 to EOF
               not at end
                   perform ECRITURE-ARTICLE-ACCUSE-RECEPTION
           end-read.

       ECRITURE-ARTICLE-ACCUSE-RECEPTION.
           perform EXTRACTION-LIGNE-COMMANDE.
           
           move corresponding Commande to Liste-Articles.
           move corresponding Article to Liste-Articles.
           move Prix of COMMANDE to Total of Liste-Articles.

           Write E-Reception-Sortie from Liste-Articles.

           add 1 to NbLigne.

           if nbligne > 55 then
               write E-Reception-Sortie from LigneEtoiles
               write E-Reception-Sortie from LigneBasDePage
               write E-Reception-Sortie from LigneEtoiles
               add 1 to NumeroPage
               write E-Reception-Sortie from LigneEtoiles
               write E-Reception-Sortie from LigneEntete
               write E-Reception-Sortie from LigneEtoiles
               move 4 to nbligne
           end-if.


       IMPRESSION-ACCUSE-RECEPTION-LIGNE-ARTICLE-END.
           move 0 to EOF.
           close F-Commande.

       IMPRESSION-FIN-ACCUSE-RECEPTION.
           perform until nbligne > 55
               write E-Reception-Sortie from " "
               add 1 to NbLigne
           end-perform.
           write E-Reception-Sortie from LigneEtoiles.
           write E-Reception-Sortie from LigneBasDePage.
           write E-Reception-Sortie from LigneEtoiles.
           
      ***************************     
      *********** PARTIE FACTURE*
      ***************************
       IMPRESSION-FACTURE.
               open input F-Facture-Entree.
               open output F-Facture-Sortie.

               perform IMPRESSION-FACTURE-PARAGRAPHE.
               perform IMPRESSION-FACTURE-LIGNE-ARTICLE.
               perform IMPRESSION-FACTURE-PARAGRAPHE.
               perform IMPRESSION-FIN-FICHIER-FACTURE.

               close F-Facture-Entree.
               close F-Facture-Sortie.

      ********** Impression paragraphe d�but jusqu'au d�but de la liste articles.  
       IMPRESSION-FACTURE-PARAGRAPHE.
           
           perform IMPRESSION-FACTURE-PARAGRAPHE-INIT.
           perform IMPRESSION-FACTURE-PARAGRAPHE-TRT until EOF = 1.
           perform IMPRESSION-FACTURE-PARAGRAPHE-END.

       IMPRESSION-FACTURE-PARAGRAPHE-INIT.
           move 0 to EOF.

      **On lit le fichier F-Facture-Entree qui servira de template au fichier de sortie.
       IMPRESSION-FACTURE-PARAGRAPHE-TRT.
           
           read F-Facture-Entree
               at end
                   move 1 to EOF
               not at end
                   perform REPLACE-FACTURE
           end-read.

      **On remplace les valeurs $ dans le fichier txt par les donn�es recup�rer dans la bdd.      
       REPLACE-FACTURE.
           inspect E-Facture-Entree tallying EstLigneArticle for all R-Liste-Article.
           if EstLigneArticle <> 0 then
               move 1 to EOF
               move 0 to EstLigneArticle
           else
               Inspect E-Facture-Entree replacing all R-NomClient by NomPrenom,
                 R-Adresse by Adresse,
                 R-CodePostal by CODE_POSTAL,
                 R-Ville by Ville,
                 R-ID-Commande by ID_COMMANDE,
                 R-Jour by Jour of DateSysteme,
                 R-Mois by Mois of DateSysteme,
                 R-Annee by Annee of DateSysteme,
                 R-Date-Commande by DATE_COMMANDE,
                 R-Total by Total-Commande-Display
           
               write E-Facture-Sortie from E-Facture-Entree
               add 1 to NbLigne

           end-if.

       IMPRESSION-FACTURE-PARAGRAPHE-END. 
           move 0 to EOF.                            

       IMPRESSION-FACTURE-LIGNE-ARTICLE.
       
           perform IMPRESSION-FACTURE-LIGNE-ARTICLE-INIT.
           perform IMPRESSION-FACTURE-LIGNE-ARTICLE-TRT until EOF = 1.
           perform IMPRESSION-FACTURE-LIGNE-ARTICLE-END.

       IMPRESSION-FACTURE-LIGNE-ARTICLE-INIT.
           move 0 to EOF.
           move 0 to Total-Commande.

           open input F-Commande.

           write E-Facture-Sortie from LigneEtoiles.
           write E-Facture-Sortie from LigneEntete.
           write E-Facture-Sortie from LigneEtoiles.
           add 3 to NbLigne

           read F-Commande
               at end
                   move 1 to EOF
           end-read.

       IMPRESSION-FACTURE-LIGNE-ARTICLE-TRT.
           read F-Commande
             at end
               move 1 to EOF
             not at end
               perform ECRITURE-ARTICLE-FACTURE
           end-read.
       
      **On lit le fichier csv de commande et on �clate la ligne pour r�cuperer les donn�es 
      **puis on move le tout dans les chamos correspondants.
      **et on �crit les lignes. 
       ECRITURE-ARTICLE-FACTURE.
           perform EXTRACTION-LIGNE-COMMANDE.

           move corresponding Commande to Liste-Articles.
           move corresponding Article to Liste-Articles.
           move PRIX of COMMANDE to Total of Liste-Articles.
           add PRIX of COMMANDE to Total-Commande.
           move Total-Commande to Total-Commande-Display.
           Write E-Facture-Sortie from Liste-Articles.
           
           add 1 to NbLigne.
           
           if nbligne > 55 then 
               write E-Facture-Sortie from LigneEtoiles
               write E-Facture-Sortie from LigneBasDePage
               write E-Facture-Sortie from LigneEtoiles
               add 1 to NumeroPage
               write E-Facture-Sortie from LigneEtoiles
               write E-Facture-Sortie from LigneEntete
               write E-Facture-Sortie from LigneEtoiles
               
               move 4 to nbligne
           end-if.
           

       IMPRESSION-FACTURE-LIGNE-ARTICLE-END.
           move 0 to EOF.
           close F-Commande.

       IMPRESSION-FIN-FICHIER-FACTURE.
           perform until nbligne > 55
               write E-facture-sortie from " "
               add 1 to NbLigne
           end-perform.
           write E-facture-sortie from LigneEtoiles.
           write E-facture-sortie from LigneBasDePage.
           write E-facture-sortie from LigneEtoiles.
           
      ***********************************************
      *******         PARTIE SQL             ********
      ***********************************************

       INIT-CONNEXION.
           move
             "Trusted_Connection=yes;Database=GestionFournisseur;server=PO-FO10\SQLEXPRESS01;factory=System.Data.SqlClient;"
             to ConnexionDB.

           exec sql
             connect using :ConnexionDB
           end-exec.
           if (sqlcode not equal 0) then
               stop run
           end-if.

           exec sql
             set autocommit on
           end-exec.

      ******** REQUETES CLIENT

       INSERT-CLIENT.
           EXEC sql
             SELECT NEWID() INTO :CLIENT.ID_CLIENT
           END-EXEC.
           EXEC sql
             INSERT INTO CLIENT VALUES
              (:CLIENT.ID_CLIENT,
               UPPER(:CLIENT.NOM),
               :CLIENT.PRENOM,
               :CLIENT.INTITULE,
               :CLIENT.TELEPHONE,
               :CLIENT.MAIL,
               :CLIENT.ADRESSE,
               :CLIENT.CODE_POSTAL,
               UPPER(:CLIENT.VILLE))
           END-EXEC.

       COUNT-CLIENT.
           EXEC sql
             SELECT COUNT(*) INTO :NombreClient FROM CLIENT 
             WHERE
               (TRIM(:CLIENT.NOM) = '' OR NOM = UPPER(:CLIENT.NOM)) AND
               (TRIM(:CLIENT.PRENOM) = '' OR PRENOM = UPPER(:CLIENT.PRENOM)) AND
               (TRIM(:CLIENT.INTITULE) = '' OR INTITULE = :CLIENT.INTITULE) AND
               (TRIM(:CLIENT.TELEPHONE) = '' OR TELEPHONE = :CLIENT.TELEPHONE) AND
               (TRIM(:CLIENT.MAIL) = '' OR MAIL = :CLIENT.MAIL) AND
               (TRIM(:CLIENT.ADRESSE) = '' OR ADRESSE = :CLIENT.ADRESSE) AND
               (TRIM(:CLIENT.CODE_POSTAL) = '' OR CODE_POSTAL = :CLIENT.CODE_POSTAL) AND
               (TRIM(:CLIENT.VILLE) = '' OR VILLE = UPPER(:CLIENT.VILLE))
           END-EXEC.
           
       SELECT-CLIENT-PRENOM-MON.
           EXEC sql
               SELECT NOM + ' ' + PRENOM INTO :NomPrenom FROM CLIENT
               WHERE ID_CLIENT = :CLIENT.ID_CLIENT
           END-EXEC.

       SELECT-CLIENT-EXCLUSIF.
           EXEC sql
             SELECT * INTO :CLIENT FROM CLIENT 
             WHERE
               (NOM = UPPER(:CLIENT.NOM)) AND
               (PRENOM = :CLIENT.PRENOM) AND
               (INTITULE = :CLIENT.INTITULE) AND
               (TELEPHONE = :CLIENT.TELEPHONE) AND
               (MAIL = :CLIENT.MAIL) AND
               (ADRESSE = UPPER(:CLIENT.ADRESSE)) AND
               (CODE_POSTAL = :CLIENT.CODE_POSTAL) AND
               (VILLE = UPPER(:CLIENT.VILLE))
           END-EXEC.

       SELECT-CLIENT-INCLUSIF.
           EXEC sql
             SELECT * INTO :CLIENT FROM CLIENT 
             WHERE
               (TRIM(:CLIENT.NOM) = '' OR NOM = UPPER(:CLIENT.NOM)) AND
               (TRIM(:CLIENT.PRENOM) = '' OR PRENOM = UPPER(:CLIENT.PRENOM)) AND
               (TRIM(:CLIENT.INTITULE) = '' OR INTITULE = :CLIENT.INTITULE) AND
               (TRIM(:CLIENT.TELEPHONE) = '' OR TELEPHONE = :CLIENT.TELEPHONE) AND
               (TRIM(:CLIENT.MAIL) = '' OR MAIL = :CLIENT.MAIL) AND
               (TRIM(:CLIENT.ADRESSE) = '' OR ADRESSE = :CLIENT.ADRESSE) AND
               (TRIM(:CLIENT.CODE_POSTAL) = '' OR CODE_POSTAL = :CLIENT.CODE_POSTAL) AND
               (TRIM(:CLIENT.VILLE) = '' OR VILLE = UPPER(:CLIENT.VILLE))
           END-EXEC.

       UPDATE-CLIENT.
           EXEC sql
             UPDATE CLIENT SET
               NOM = UPPER(:CLIENT.NOM),
               PRENOM = :CLIENT.PRENOM,
               INTITULE = :CLIENT.INTITULE,
               TELEPHONE = :CLIENT.TELEPHONE,
               MAIL = :CLIENT.MAIL,
               ADRESSE = :CLIENT.ADRESSE,
               CODE_POSTAL = :CLIENT.CODE_POSTAL,
               VILLE = UPPER(:CLIENT.VILLE)
             WHERE (ID_CLIENT = :CLIENT.ID_CLIENT)
           END-EXEC.

       DELETE-CLIENT.
           EXEC sql
             DELETE FROM CLIENT WHERE ID_CLIENT = :CLIENT.ID_CLIENT
           END-EXEC.

       INIT-CURSOR-CLIENT.
           EXEC sql
             DECLARE CLIENT_CURSOR SCROLL CURSOR FOR
             SELECT * FROM CLIENT
             WHERE 
               (TRIM(:CLIENT.NOM) = '' OR NOM = :CLIENT.NOM) AND
               (TRIM(:CLIENT.PRENOM) = '' OR PRENOM = :CLIENT.PRENOM) AND
               (TRIM(:CLIENT.INTITULE) = '' OR INTITULE = :CLIENT.INTITULE) AND
               (TRIM(:CLIENT.TELEPHONE) = '' OR TELEPHONE = :CLIENT.TELEPHONE) AND
               (TRIM(:CLIENT.MAIL) = '' OR MAIL = :CLIENT.MAIL) AND
               (TRIM(:CLIENT.ADRESSE) = '' OR ADRESSE = :CLIENT.ADRESSE) AND
               (TRIM(:CLIENT.CODE_POSTAL) = '' OR CODE_POSTAL = :CLIENT.CODE_POSTAL) AND
               (TRIM(:CLIENT.VILLE) = '' OR VILLE = :CLIENT.VILLE)
             ORDER BY NOM
           END-EXEC.
           EXEC sql
             OPEN CLIENT_CURSOR
           END-EXEC.

       FETCH-CURSOR-CLIENT.
           EXEC sql
             FETCH CLIENT_CURSOR INTO :CLIENT
           END-EXEC.

       FETCH-CURSOR-ABSOLUTE-CLIENT.
           EXEC sql
             FETCH ABSOLUTE :SQL-index-cursor FROM CLIENT_CURSOR INTO :CLIENT
           END-EXEC.

       CLOSE-CURSOR-CLIENT.
           EXEC sql
             CLOSE CLIENT_CURSOR
           END-EXEC.


      ******** REQUETES ARTICLE
       
       INSERT-ARTICLE.
           EXEC sql
             SELECT NEWID() INTO :ARTICLE.ID_ARTICLE
           END-EXEC.
           EXEC sql
             INSERT INTO ARTICLE VALUES
              (:ARTICLE.ID_ARTICLE,
               :ARTICLE.NOM,
               :ARTICLE.PRIX,
               :ARTICLE.STOCK,
               :ARTICLE.STOCK_MIN)
           END-EXEC.

       COUNT-ARTICLE.
           EXEC sql
             SELECT COUNT(*) INTO :NombreArticle FROM ARTICLE
             WHERE
               (TRIM(:ARTICLE.NOM) = '' OR NOM = :ARTICLE.NOM) AND
               (TRIM(:TEST_PRIX) = '' OR 
                   ((PRIX > :ARTICLE.PRIX AND :TEST_PRIX = '+') OR
                    (PRIX = :ARTICLE.PRIX AND :TEST_PRIX = '=') OR
                    (PRIX < :ARTICLE.PRIX AND :TEST_PRIX = '-')))
               AND
               (TRIM(:TEST_STOCK) = '' OR 
                   ((STOCK > :ARTICLE.STOCK_MIN AND :TEST_STOCK = '+') OR
                    (STOCK = :ARTICLE.STOCK_MIN AND :TEST_STOCK = '=') OR
                    (STOCK < :ARTICLE.STOCK_MIN AND :TEST_STOCK = '-')))
               AND
               (TRIM(:TEST_STOCK_MIN) = '' OR
                   ((STOCK_MIN > :ARTICLE.STOCK_MIN AND :TEST_STOCK_MIN = '+') OR
                    (STOCK_MIN = :ARTICLE.STOCK_MIN AND :TEST_STOCK_MIN = '=') OR
                    (STOCK_MIN < :ARTICLE.STOCK_MIN AND :TEST_STOCK_MIN = '-')))
               AND (TRIM(:TEST_STOCK_SUP_MIN) = '' OR (:TEST_STOCK_SUP_MIN = 'Y' AND STOCK_MIN < STOCK))
               AND (TRIM(:TEST_STOCK_INF_MIN) = '' OR (:TEST_STOCK_INF_MIN = 'Y' AND STOCK_MIN > STOCK))
           END-EXEC.

       SELECT-ARTICLE-BY-ID.
           EXEC sql
             SELECT * INTO :ARTICLE FROM ARTICLE
             WHERE ID_ARTICLE = :ARTICLE.ID_ARTICLE
           END-EXEC.
          
       SELECT-ARTICLE-INCLUSIF.
           EXEC sql
             SELECT * INTO :ARTICLE FROM ARTICLE 
             WHERE
               (TRIM(:ARTICLE.NOM) = '' OR NOM = :ARTICLE.NOM) AND
               (TRIM(:TEST_PRIX) = '' OR
                   ((PRIX > :ARTICLE.PRIX AND :TEST_PRIX = '+') OR
                    (PRIX = :ARTICLE.PRIX AND :TEST_PRIX = '=') OR
                    (PRIX < :ARTICLE.PRIX AND :TEST_PRIX = '-')))
               AND
               (TRIM(:TEST_STOCK) = '' OR
                   ((STOCK > :ARTICLE.STOCK AND :TEST_STOCK = '+') OR
                    (STOCK = :ARTICLE.STOCK AND :TEST_STOCK = '=') OR
                    (STOCK < :ARTICLE.STOCK AND :TEST_STOCK = '-')))
               AND
               (TRIM(:TEST_STOCK_MIN) = '' OR
                   ((STOCK_MIN > :ARTICLE.STOCK_MIN AND :TEST_STOCK_MIN = '+') OR
                    (STOCK_MIN = :ARTICLE.STOCK_MIN AND :TEST_STOCK_MIN = '=') OR
                    (STOCK_MIN < :ARTICLE.STOCK_MIN AND :TEST_STOCK_MIN = '-')))
               AND (TRIM(:TEST_STOCK_SUP_MIN) = '' OR (:TEST_STOCK_SUP_MIN = 'Y' AND STOCK_MIN < STOCK))
               AND (TRIM(:TEST_STOCK_INF_MIN) = '' OR (:TEST_STOCK_INF_MIN = 'Y' AND STOCK_MIN > STOCK))
           END-EXEC.

       UPDATE-ARTICLE.
           EXEC sql
             UPDATE ARTICLE SET
               NOM = :ARTICLE.NOM,
               PRIX = :ARTICLE.PRIX,
               STOCK = :ARTICLE.STOCK,
               STOCK_MIN = :ARTICLE.STOCK_MIN
             WHERE (ID_ARTICLE = :ARTICLE.ID_ARTICLE)
           END-EXEC.

       DELETE-ARTICLE.
           EXEC sql
             DELETE FROM ARTICLE WHERE ID_ARTICLE = :ARTICLE.ID_ARTICLE
           END-EXEC.

       INIT-CURSOR-ARTICLE.
           EXEC sql
             DECLARE ARTICLE_CURSOR SCROLL CURSOR FOR
             SELECT * FROM ARTICLE
             WHERE
               (TRIM(:ARTICLE.NOM) = '' OR NOM = :ARTICLE.NOM) AND
               (TRIM(:TEST_PRIX) = '' OR
                   ((PRIX > :ARTICLE.PRIX AND :TEST_PRIX = '+') OR
                    (PRIX = :ARTICLE.PRIX AND :TEST_PRIX = '=') OR
                    (PRIX < :ARTICLE.PRIX AND :TEST_PRIX = '-')))
               AND
               (TRIM(:TEST_STOCK) = '' OR
                   ((STOCK > :ARTICLE.STOCK AND :TEST_STOCK = '+') OR
                    (STOCK = :ARTICLE.STOCK AND :TEST_STOCK = '=') OR
                    (STOCK < :ARTICLE.STOCK AND :TEST_STOCK = '-')))
               AND
               (TRIM(:TEST_STOCK_MIN) = '' OR
                   ((STOCK_MIN > :ARTICLE.STOCK_MIN AND :TEST_STOCK_MIN = '+') OR
                    (STOCK_MIN = :ARTICLE.STOCK_MIN AND :TEST_STOCK_MIN = '=') OR
                    (STOCK_MIN < :ARTICLE.STOCK_MIN AND :TEST_STOCK_MIN = '-')))
               AND (TRIM(:TEST_STOCK_SUP_MIN) = '' OR (:TEST_STOCK_SUP_MIN = 'Y' AND STOCK_MIN < STOCK))
               AND (TRIM(:TEST_STOCK_INF_MIN) = '' OR (:TEST_STOCK_INF_MIN = 'Y' AND STOCK_MIN > STOCK))
             ORDER BY NOM
           END-EXEC.
           EXEC sql
             OPEN ARTICLE_CURSOR
           END-EXEC.

       FETCH-CURSOR-ABSOLUTE-ARTICLE.
           EXEC sql
             FETCH ABSOLUTE :SQL-index-cursor FROM ARTICLE_CURSOR INTO :ARTICLE
           END-EXEC.

       FETCH-CURSOR-ARTICLE.
           EXEC sql
             FETCH ARTICLE_CURSOR INTO :ARTICLE
           END-EXEC.
           
       CLOSE-CURSOR-ARTICLE.
           EXEC sql
             CLOSE ARTICLE_CURSOR
           END-EXEC.

       end program Program1.
